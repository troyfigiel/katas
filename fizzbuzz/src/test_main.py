import pytest

from main import fizzbuzz


@pytest.mark.parametrize("n", [1, 2])
def test_should_return_number_if_not_multiple_of_three_five_fifteen(n):
    assert fizzbuzz(n) == n


@pytest.mark.parametrize("n", [3, 6])
def test_should_return_fizz_for_multiple_of_three_but_not_fifteen(n):
    assert fizzbuzz(n) == "Fizz"


@pytest.mark.parametrize("n", [5, 10])
def test_should_return_buzz_for_multiples_of_five_but_not_fifteen(n):
    assert fizzbuzz(n) == "Buzz"


@pytest.mark.parametrize("n", [15, 30])
def test_should_return_fizzbuzz_for_multiples_of_fifteen(n):
    assert fizzbuzz(n) == "FizzBuzz"

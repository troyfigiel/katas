START_CUSTOM_DELIMITER = "//"
END_CUSTOM_DELIMITER = "\n"


def add(s):
    if not s:
        return 0
    if s.startswith(START_CUSTOM_DELIMITER):
        return _add_with_custom_delimiter(s)
    return _add_with_default_delimiters(s)


def _add_with_custom_delimiter(s):
    delimiter = _read_custom_delimiter(s)
    remaining_string = _remove_custom_delimiter_from_string(s, delimiter)
    return _calculate_custom_delimited_sum(remaining_string, delimiter)


def _read_custom_delimiter(s):
    return s[2 : s.find(END_CUSTOM_DELIMITER)]


def _remove_custom_delimiter_from_string(s, delimiter):
    return s[len(START_CUSTOM_DELIMITER) + len(delimiter) + 1 :]


def _calculate_custom_delimited_sum(remaining_string, delimiter):
    try:
        return _calculate_delimited_sum(remaining_string, delimiter)
    except ValueError:
        position, character = _find_unexpected_character(
            remaining_string, delimiter
        )
        raise UnexpectedCharacterError(
            f"'{delimiter}' expected but '{character}' "
            + f"found at position {position}."
        )


def _find_unexpected_character(s, delimiter):
    for position, character in enumerate(s):
        if character.isdigit() or character == delimiter:
            continue
        return position, character


def _add_with_default_delimiters(s):
    replaced_string = s.replace("\n", ",")
    if "," not in replaced_string:
        return int(replaced_string)
    if replaced_string[-1] == ",":
        raise EndingSeparatorError
    return _calculate_delimited_sum(replaced_string, ",")


def _calculate_delimited_sum(s, delimiter):
    return sum(int(n) for n in s.split(delimiter))


class EndingSeparatorError(ValueError):
    """Raised when input string ends with a separator."""


class UnexpectedCharacterError(ValueError):
    """Raised when a non-delimiter, non-numeric character is encountered."""

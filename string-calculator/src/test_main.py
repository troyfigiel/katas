import pytest

from main import EndingSeparatorError, add, UnexpectedCharacterError


def test_should_return_zero_empty_string():
    assert add("") == 0


@pytest.mark.parametrize(["s", "n"], [("1", 1), ("9", 9), ("19", 19)])
def test_should_return_integer_for_single_number(s, n):
    assert add(s) == n


@pytest.mark.parametrize(["s", "n"], [("1,2", 3), ("3,2", 5), ("11,4", 15)])
def test_should_return_sum_for_two_numbers_separated_by_comma(s, n):
    assert add(s) == n


def test_should_return_six_for_one_two_three():
    assert add("1,2,3") == 6


def test_should_return_six_for_one_two_three_with_newlines():
    assert add("1\n2\n3") == 6


def test_should_return_six_for_one_two_three_with_mixed_commas_newlines():
    assert add("1,2\n3") == 6


def test_should_raise_error_if_comma_at_end():
    with pytest.raises(EndingSeparatorError):
        add("1,2,")


def test_should_allow_specifying_semicolon_delimiter():
    assert add("//;\n1;2") == 3


def test_should_allow_specifying_string_delimiter():
    assert add("//sep\n5sep11sep190") == 206


def test_should_raise_error_when_string_other_than_custom_delimiter_used():
    with pytest.raises(
        UnexpectedCharacterError,
        match=r"'|' expected but ',' found at position 3.",
    ):
        add("//|\n1|2,3")

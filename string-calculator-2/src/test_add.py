from unittest.mock import call, Mock
import pytest

from add import add


def test_should_add_empty_string():
    assert add("") == 0


@pytest.mark.parametrize(["numbers", "expected"], [("2", 2), ("17", 17)])
def test_should_add_single_number(numbers, expected):
    assert add(numbers) == expected


@pytest.mark.parametrize(["numbers", "expected"], [("1,2", 3), ("17,9", 26)])
def test_should_add_two_numbers_separated_by_comma(numbers, expected):
    assert add(numbers) == expected


def test_should_add_many_numbers_separated_by_comma():
    assert add("12,3,192,1") == 208


@pytest.mark.parametrize(["numbers", "expected"], [("1,2\n3", 6), ("3\n5\n18", 26)])
def test_should_handle_newlines(numbers, expected):
    assert add(numbers) == expected


@pytest.mark.parametrize(
    ["numbers", "expected"], [("//;\n1;2", 3), ("//sep\n3sep12sep109", 124)]
)
def test_should_handle_custom_delimiter(numbers, expected):
    assert add(numbers) == expected


def test_should_not_allow_negative_numbers():
    with pytest.raises(ValueError, match="Negative values not allowed: -1"):
        add("-1,2")


def test_should_list_all_negative_numbers_in_error():
    with pytest.raises(ValueError, match="Negative values not allowed: -1, -2"):
        add("//x;\n3x;-1x;-2")


def test_should_ignore_numbers_larger_than_thousand():
    assert add("//sep\n1001sep2sep35") == 37


def test_should_log_every_call():
    logger = Mock()
    add(numbers="1,2,3", logger=logger)
    assert logger.mock_calls == [call.write(6)]

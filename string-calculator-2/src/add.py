from typing import Optional

CUSTOM_DELIMITER_PREFIX = "//"
CUSTOM_DELIMITER_SUFFIX = "\n"

CommaStringSum = str
IntList = list[int]
PositiveIntList = list[int]
PositiveIntListUnderLimit = list[int]


class Logger:
    pass


def add(numbers: str, logger: Optional[Logger] = None) -> int:
    sum_value = sum(_parse_numbers(numbers))
    if logger:
        logger.write(sum_value)
    return sum_value


def _parse_numbers(numbers: str) -> PositiveIntListUnderLimit:
    comma_string_sum = _parse_comma_string_sum(numbers)
    int_list = _parse_int_list(comma_string_sum)
    positive_int_list = _parse_positive_integers(int_list)
    return _parse_small_positive_integers(positive_int_list)


def _parse_comma_string_sum(numbers: str) -> CommaStringSum:
    if not numbers.startswith(CUSTOM_DELIMITER_PREFIX):
        return numbers.replace("\n", ",")
    delimiter = numbers[: numbers.find(CUSTOM_DELIMITER_SUFFIX)].strip(
        CUSTOM_DELIMITER_PREFIX
    )
    content_start = numbers.find(CUSTOM_DELIMITER_SUFFIX) + len(CUSTOM_DELIMITER_SUFFIX)
    return numbers[content_start:].replace(delimiter, ",")


def _parse_int_list(comma_string_sum: CommaStringSum) -> IntList:
    if not comma_string_sum:
        return []
    return [int(x) for x in comma_string_sum.split(",")]


def _parse_positive_integers(ints: IntList) -> PositiveIntList:
    negative_ints = [x for x in ints if x < 0]
    if not negative_ints:
        return ints
    raise ValueError(
        f"Negative values not allowed: {', '.join(map(str, negative_ints))}"
    )


def _parse_small_positive_integers(
    ints: PositiveIntList, limit: int = 1000
) -> PositiveIntListUnderLimit:
    return [x for x in ints if x <= limit]

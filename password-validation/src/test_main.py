from main import validate
import pytest


def test_should_be_at_least_eight_characters():
    with pytest.raises(
        ValueError, match=r"^Password must be at least 8 characters$"
    ):
        validate("Pass123")


def test_should_contain_at_least_two_numbers():
    with pytest.raises(
        ValueError, match=r"^The password must contain at least 2 numbers$"
    ):
        validate("myPassword1")


def test_should_show_both_number_and_character_errors():
    with pytest.raises(
        ValueError,
        match="^Password must be at least 8 characters\nThe password must contain at least 2 numbers$",
    ):
        validate("Pass")

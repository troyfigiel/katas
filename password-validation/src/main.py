def validate(password):
    if _too_few_digits(password) and _too_few_characters(password):
        raise ValueError(
            "Password must be at least 8 characters\nThe password must contain at least 2 numbers"
        )
    if _too_few_characters(password):
        raise ValueError("Password must be at least 8 characters")
    if _too_few_digits(password):
        raise ValueError("The password must contain at least 2 numbers")


def _too_few_characters(password):
    return len(password) < 8


def _too_few_digits(password):
    return sum(char.isdigit() for char in password) < 2

import pytest

from main import execute

# We have squares
# Each square can have an object or not
# The rover can look one square ahead
# Each "square" is defined by longitudinal and latitudinal curves
# We need to be able to give the rover a series of commands


@pytest.mark.parametrize(
    ["initial_parameters", "commands", "expected_parameters"],
    [
        (((1, 2), "N"), "l", ((1, 2), "W")),
        (((2, 0), "N"), "ll", ((2, 0), "S")),
        (((0, 0), "N"), "lll", ((0, 0), "E")),
        (((2, 2), "N"), "llll", ((2, 2), "N")),
    ],
)
def test_should_turn_left(initial_parameters, commands, expected_parameters):
    assert execute(initial_parameters, commands) == expected_parameters


@pytest.mark.parametrize(
    ["initial_parameters", "commands", "expected_parameters"],
    [
        (((1, 2), "N"), "r", ((1, 2), "E")),
        (((0, 1), "N"), "rr", ((0, 1), "S")),
        (((0, 2), "N"), "rrr", ((0, 2), "W")),
        (((2, 0), "N"), "rrrr", ((2, 0), "N")),
    ],
)
def test_should_turn_right(initial_parameters, commands, expected_parameters):
    assert execute(initial_parameters, commands) == expected_parameters


@pytest.mark.parametrize(
    ["initial_parameters", "commands", "expected_parameters"],
    [
        (((1, 0), "N"), "f", ((1, 1), "N")),
        (((0, 0), "N"), "ff", ((0, 2), "N")),
        (((0, 1), "E"), "f", ((1, 1), "E")),
        (((0, 0), "E"), "ff", ((2, 0), "E")),
        (((1, 2), "S"), "f", ((1, 1), "S")),
        (((2, 2), "S"), "ff", ((2, 0), "S")),
        (((1, 0), "W"), "f", ((0, 0), "W")),
        (((2, 2), "W"), "ff", ((0, 2), "W")),
    ],
)
def test_should_move_forward(initial_parameters, commands, expected_parameters):
    assert execute(initial_parameters, commands) == expected_parameters


@pytest.mark.parametrize(
    ["initial_parameters", "commands", "expected_parameters"],
    [
        (((0, 2), "N"), "b", ((0, 1), "N")),
        (((1, 2), "N"), "bb", ((1, 0), "N")),
        (((1, 2), "E"), "b", ((0, 2), "E")),
        (((2, 0), "E"), "bb", ((0, 0), "E")),
        (((2, 0), "S"), "b", ((2, 1), "S")),
        (((0, 0), "S"), "bb", ((0, 2), "S")),
        (((1, 1), "W"), "b", ((2, 1), "W")),
        (((0, 1), "W"), "bb", ((2, 1), "W")),
    ],
)
def test_should_move_backward(initial_parameters, commands, expected_parameters):
    assert execute(initial_parameters, commands) == expected_parameters


@pytest.mark.parametrize(
    ["initial_parameters", "commands", "expected_parameters"],
    [
        (((2, 9), "N"), "f", ((7, 9), "S")),
        (((6, 9), "N"), "f", ((1, 9), "S")),
        (((3, 9), "S"), "b", ((8, 9), "N")),
        (((9, 9), "S"), "b", ((4, 9), "N")),
    ],
)
def test_should_wrap_around_north_pole(
    initial_parameters, commands, expected_parameters
):
    assert execute(initial_parameters, commands) == expected_parameters


@pytest.mark.parametrize(
    ["initial_parameters", "commands", "expected_parameters"],
    [
        (((2, 0), "S"), "f", ((7, 0), "N")),
        (((6, 0), "S"), "f", ((1, 0), "N")),
        (((3, 0), "N"), "b", ((8, 0), "S")),
        (((9, 0), "N"), "b", ((4, 0), "S")),
    ],
)
def test_should_wrap_around_south_pole(
    initial_parameters, commands, expected_parameters
):
    assert execute(initial_parameters, commands) == expected_parameters


@pytest.mark.parametrize(
    ["initial_parameters", "commands", "expected_parameters"],
    [
        (((0, 3), "W"), "f", ((9, 3), "W")),
        (((9, 1), "E"), "f", ((0, 1), "E")),
    ],
)
def test_should_wrap_around_equator(initial_parameters, commands, expected_parameters):
    assert execute(initial_parameters, commands) == expected_parameters

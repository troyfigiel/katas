from dataclasses import dataclass
from enum import Enum
from functools import partial
from typing import NamedTuple

from bidict import bidict

MAP_SIZE = {"x": 10, "y": 10}


def execute(
    parameters: tuple[tuple[int, int], str], commands: str
) -> tuple[tuple[int, int], str]:
    rover = parse_rover(parameters)
    for command in commands:
        rover.execute(command)
    return rover.parameters()


class RoverLocation(NamedTuple):
    x: int
    y: int


class RoverOrientation(Enum):
    NORTH = 0
    EAST = 1
    SOUTH = 2
    WEST = 3


ORIENTATION_BY_SYMBOL = bidict(
    {
        "N": RoverOrientation.NORTH,
        "E": RoverOrientation.EAST,
        "S": RoverOrientation.SOUTH,
        "W": RoverOrientation.WEST,
    }
)


class TurnDirection(Enum):
    RIGHT = 1
    LEFT = -1


class MoveDirection(Enum):
    FORWARD = 1
    BACKWARD = -1


class PoleWrapping(Enum):
    NORTH = -1
    SOUTH = 1


@dataclass
class Rover:
    location: RoverLocation
    orientation: RoverOrientation

    def execute(self, command: str) -> None:
        {
            "l": partial(self._turn, TurnDirection.LEFT),
            "r": partial(self._turn, TurnDirection.RIGHT),
            "f": partial(self._move, MoveDirection.FORWARD),
            "b": partial(self._move, MoveDirection.BACKWARD),
        }[command]()

    def _turn(self, direction: TurnDirection) -> None:
        self.orientation = RoverOrientation(
            (self.orientation.value + direction.value) % len(RoverOrientation)
        )

    def _unwrapped_location_after_move(self, direction: MoveDirection) -> RoverLocation:
        return {
            RoverOrientation.NORTH: RoverLocation(
                self.location.x, self.location.y + direction.value
            ),
            RoverOrientation.EAST: RoverLocation(
                self.location.x + direction.value, self.location.y
            ),
            RoverOrientation.SOUTH: RoverLocation(
                self.location.x, self.location.y - direction.value
            ),
            RoverOrientation.WEST: RoverLocation(
                self.location.x - direction.value, self.location.y
            ),
        }[self.orientation]

    def _move(self, direction: MoveDirection) -> None:
        self.location = self._unwrapped_location_after_move(direction)
        wrapping_function = {
            -1: partial(self._wrap_location_crossing_pole, PoleWrapping.SOUTH),
            MAP_SIZE["x"]: partial(
                self._wrap_location_crossing_pole, PoleWrapping.NORTH
            ),
        }
        if self.location.y in (-1, MAP_SIZE["y"]):
            self.location = wrapping_function[self.location.y]()
            self.orientation = self._wrap_orientation_crossing_poles()
        if self.location.x == -1:
            self.location = self._wrap_location_crossing_equator()
        if self.location.x == MAP_SIZE["x"]:
            self.location = self._wrap_location_crossing_equator()

    def _wrap_location_crossing_pole(self, pole: PoleWrapping):
        wrapped_x = (self.location.x + MAP_SIZE["x"] / 2) % MAP_SIZE["x"]
        wrapped_y = self.location.y + pole.value
        return RoverLocation(wrapped_x, wrapped_y)

    def _wrap_location_crossing_equator(self):
        return RoverLocation(self.location.x % MAP_SIZE["x"], self.location.y)

    def _wrap_orientation_crossing_poles(self):
        if self.orientation == RoverOrientation.NORTH:
            return RoverOrientation.SOUTH
        if self.orientation == RoverOrientation.SOUTH:
            return RoverOrientation.NORTH

    def parameters(self) -> tuple[tuple[int, int], str]:
        return (
            self.location,
            ORIENTATION_BY_SYMBOL.inverse[self.orientation],
        )


def parse_rover(parameters: tuple[tuple[int, int], str]) -> Rover:
    return Rover(
        location=RoverLocation(*parameters[0]),
        orientation=ORIENTATION_BY_SYMBOL[parameters[1]],
    )

from gilded_rose import GildedRose, Item
from enum import Enum
from hypothesis import assume, given, strategies as st
from pydantic import BaseModel, PositiveInt, Field


class SpecialItemNames(Enum):
    AGED_BRIE = "Aged Brie"
    BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert"
    SULFURAS = "Sulfuras, Hand of Ragnaros"


SPECIAL_ITEM_NAMES = [e.value for e in SpecialItemNames]


class ItemValidator(BaseModel):
    name: str
    sell_in: int
    quality: int = Field(ge=0, le=50)


@st.composite
def valid_item(
    draw,
    name=st.one_of(
        st.sampled_from(SPECIAL_ITEM_NAMES),
        st.text(),
    ),
    sell_in=...,
    quality=...,
):
    return draw(st.builds(ItemValidator, name=name, sell_in=sell_in, quality=quality))


def _update(valid_item: ItemValidator) -> Item:
    item = Item(**valid_item.dict())
    GildedRose([item]).update_quality()
    return item


# The Quality of an item is never negative
# The Quality of an item is never more than 50
@given(valid_item())
def test_valid_item_should_remain_valid_after_update(valid_item):
    item = _update(valid_item)
    ItemValidator(**item.__dict__)


# At the end of each day our system lowers both values for every item
@given(valid_item())
def test_sell_in_should_always_decrease_by_one_except_for_sulfuras(valid_item):
    assume(valid_item.name != SpecialItemNames.SULFURAS.value)
    assert _update(valid_item).sell_in == valid_item.sell_in - 1


@given(
    valid_item(
        name=st.text(),
        sell_in=st.integers(min_value=1),
    )
)
def test_normal_item_should_decrease_in_quality_by_one_before_sell_date(valid_item):
    assert _update(valid_item).quality == max(valid_item.quality - 1, 0)


@given(
    valid_item(
        name=st.text(),
        sell_in=st.integers(max_value=0),
    )
)
def test_normal_item_should_decrease_in_quality_by_two_after_sell_date(valid_item):
    assert _update(valid_item).quality == max(valid_item.quality - 2, 0)


# "Sulfuras", being a legendary item, never has to be sold or decreases in Quality
@given(valid_item(name=st.just(SpecialItemNames.SULFURAS.value)))
def test_sulfuras_should_not_decrease_in_sell_in(valid_item):
    assert _update(valid_item).sell_in == valid_item.sell_in


@given(valid_item(name=st.just(SpecialItemNames.SULFURAS.value)))
def test_sulfuras_should_not_decrease_in_quality(valid_item):
    assert _update(valid_item).quality == valid_item.quality


# "Aged Brie" actually increases in Quality the older it gets
@given(
    valid_item(
        name=st.just(SpecialItemNames.AGED_BRIE.value),
        sell_in=st.integers(min_value=1),
    )
)
def test_aged_brie_should_increase_in_quality_by_one_before_sell_date(valid_item):
    assert _update(valid_item).quality == min(valid_item.quality + 1, 50)


@given(
    valid_item(
        name=st.just(SpecialItemNames.AGED_BRIE.value),
        sell_in=st.integers(max_value=0),
    )
)
def test_aged_brie_should_increase_in_quality_by_two_after_sell_date(valid_item):
    assert _update(valid_item).quality == min(valid_item.quality + 2, 50)


# "Backstage passes", like aged brie, increases in Quality as its SellIn value
# approaches; Quality increases by 2 when there are 10 days or less and by 3 when there
# are 5 days or less but Quality drops to 0 after the concert
@given(
    valid_item(
        name=st.just(SpecialItemNames.BACKSTAGE_PASSES.value),
        sell_in=st.integers(min_value=11),
    )
)
def test_backstage_passes_should_increase_in_quality_by_1_with_more_than_10_days_left(
    valid_item,
):
    assert _update(valid_item).quality == min(valid_item.quality + 1, 50)


@given(
    valid_item(
        name=st.just(SpecialItemNames.BACKSTAGE_PASSES.value),
        sell_in=st.integers(min_value=6, max_value=10),
    )
)
def test_backstage_passes_should_increase_in_quality_by_2_with_5_to_10_days_left(
    valid_item,
):
    assert _update(valid_item).quality == min(valid_item.quality + 2, 50)


@given(
    valid_item(
        name=st.just(SpecialItemNames.BACKSTAGE_PASSES.value),
        sell_in=st.integers(min_value=1, max_value=5),
    )
)
def test_backstage_passes_should_increase_in_quality_by_3_with_less_than_5_days_left(
    valid_item,
):
    assert _update(valid_item).quality == min(valid_item.quality + 3, 50)


@given(
    valid_item(
        name=st.just(SpecialItemNames.BACKSTAGE_PASSES.value),
        sell_in=st.integers(max_value=0),
    )
)
def test_backstage_passes_should_have_no_quality_after_sell_date(
    valid_item,
):
    assert _update(valid_item).quality == 0

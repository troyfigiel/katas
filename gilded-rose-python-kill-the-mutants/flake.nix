{
  description = "Template for a poetry2nix project";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:nixos/nixpkgs";
  };

  outputs = inputs:
    let inherit (inputs.flake-utils.lib) eachDefaultSystem;
    in eachDefaultSystem (system: rec {
      pkgs = import inputs.nixpkgs { inherit system; };
      devShells.default = pkgs.mkShell {
        buildInputs = (with pkgs; [ black mutmut mypy nixfmt refurb ruff ])
          ++ (with pkgs.python3Packages; [ coverage hypothesis pytest ]);
      };
    });
}

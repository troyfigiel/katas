import pytest

from gilded_rose import Item, GildedRose


@pytest.mark.parametrize(
    ["item", "expected"],
    [
        # At the end of each day our system lowers both values for every item
        (Item("Random item", 3, 4), "Random item, 2, 3"),
        # Once the sell by date has passed, Quality degrades twice as fast
        (Item("Random item", 0, 4), "Random item, -1, 2"),
        # The Quality of an item is never negative
        (Item("Random item", 0, 0), "Random item, -1, 0"),
        # "Aged Brie" actually increases in Quality the older it gets
        (Item("Aged Brie", 3, 7), "Aged Brie, 2, 8"),
        # The Quality of an item is never more than 50
        (Item("Aged Brie", 3, 50), "Aged Brie, 2, 50"),
        # "Sulfuras", being a legendary item, never has to be sold or decreases
        # in Quality
        (
            Item("Sulfuras, Hand of Ragnaros", 0, 30),
            "Sulfuras, Hand of Ragnaros, 0, 30",
        ),
        # "Backstage passes", like aged brie, increases in Quality as its SellIn value
        # approaches;
        (
            Item("Backstage passes to a TAFKAL80ETC concert", 15, 3),
            "Backstage passes to a TAFKAL80ETC concert, 14, 4",
        ),
        # Quality increases by 2 when there are 10 days or less
        (
            Item("Backstage passes to a TAFKAL80ETC concert", 10, 3),
            "Backstage passes to a TAFKAL80ETC concert, 9, 5",
        ),
        # and by 3 when there are 5 days or less but
        (
            Item("Backstage passes to a TAFKAL80ETC concert", 5, 5),
            "Backstage passes to a TAFKAL80ETC concert, 4, 8",
        ),
        # Quality drops to 0 after the concert
        (
            Item("Backstage passes to a TAFKAL80ETC concert", 0, 7),
            "Backstage passes to a TAFKAL80ETC concert, -1, 0",
        ),
        # Finally, to get full coverage we actually need the following test case.
        # This is potentially a bug, since it is not described in the requirements.
        (Item("Aged Brie", -4, 48), "Aged Brie, -5, 50"),
        # To add the new requirement for conjured items, we add tests.
        # "Conjured" items degrade in Quality twice as fast as normal items
        (Item("Conjured stick", 7, 3), "Conjured stick, 6, 1"),
        (Item("Conjured stick", 7, 3), "Conjured stick, 6, 1"),
        # The following test cases are needed to kill most mutants. We will still have some false
        # positives due to equivalent mutations. For example '0 if x < 0 else x' is equivalent to
        # '0 if x <= 0 else x'. The difference between these two pieces of codes is untestable.
        # Equivalent mutations do not mean the tests are not sufficient!
        (Item("Aged Brie", 0, 37), "Aged Brie, -1, 38"),
        (Item("Aged Brie", -1, 37), "Aged Brie, -2, 39"),
        (
            Item("Backstage passes to a TAFKAL80ETC concert", 11, 3),
            "Backstage passes to a TAFKAL80ETC concert, 10, 4",
        ),
        (
            Item("Backstage passes to a TAFKAL80ETC concert", 6, 3),
            "Backstage passes to a TAFKAL80ETC concert, 5, 5",
        ),
        (
            Item("Backstage passes to a TAFKAL80ETC concert", 1, 3),
            "Backstage passes to a TAFKAL80ETC concert, 0, 6",
        ),
        (Item("Random item", 1, 4), "Random item, 0, 3"),
    ],
)
def test_update_single_item(item, expected):
    gilded_rose = GildedRose([item])
    gilded_rose.update_quality()
    assert repr(gilded_rose.items[0]) == expected

from enum import Enum
from typing import Callable


class GildedRose(object):
    def __init__(self, items):
        self.items = items

    def update_quality(self):
        for item in self.items:
            updater = {
                "Aged Brie": _aged_brie_updater,
                "Backstage passes to a TAFKAL80ETC concert": _backstage_passes_updater,
                "Sulfuras, Hand of Ragnaros": _sulfuras_updater,
            }.get(item.name, _default_update_strategy(_parse_degradation_speed(item)))
            updater(item)


class Item:
    def __init__(self, name, sell_in, quality):
        self.name = name
        self.sell_in = sell_in
        self.quality = quality

    def __repr__(self):
        return "%s, %s, %s" % (self.name, self.sell_in, self.quality)


class DegradationSpeed(Enum):
    NORMAL = 1
    CONJURED = 2


def _parse_degradation_speed(item: Item) -> DegradationSpeed:
    if item.name.startswith("Conjured"):
        return DegradationSpeed.CONJURED
    return DegradationSpeed.NORMAL


Updater = Callable[[Item], Item]  # pragma: no mutate


def _aged_brie_updater(item: Item) -> None:
    item.quality += 1
    if item.sell_in < 0:
        item.quality += 1
    if item.quality > 50:
        item.quality = 50
    item.sell_in -= 1


def _backstage_passes_updater(item: Item) -> None:
    item.quality += 1
    if item.sell_in <= 10:
        item.quality += 1
    if item.sell_in <= 5:
        item.quality += 1
    if item.sell_in <= 0:
        item.quality = 0
    item.sell_in -= 1


def _sulfuras_updater(item: Item) -> None:
    pass


def _default_update_strategy(
    degradation_speed: DegradationSpeed = DegradationSpeed.NORMAL,
) -> Updater:
    def updater(item: Item) -> None:
        item.quality -= degradation_speed.value
        if item.sell_in <= 0:
            item.quality -= degradation_speed.value
        if item.quality < 0:
            item.quality = 0
        item.sell_in -= 1

    return updater

{
  description = "Template for a poetry2nix project";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:nixos/nixpkgs";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.flake-utils.follows = "flake-utils";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs:
    let inherit (inputs.flake-utils.lib) eachSystem system;
    in eachSystem (with system; [ x86_64-linux aarch64-linux ]) (system: rec {
      pkgs = import inputs.nixpkgs {
        inherit system;
        overlays = [
          inputs.poetry2nix.overlay
          (_final: prev: {
            virtualEnv = (prev.poetry2nix.mkPoetryEnv {
              projectDir = ./.;
            }).env.overrideAttrs (oldAttrs: {
              buildInputs = [ pkgs.python3Packages.jedi-language-server ];
            });
          })
        ];
      };
      devShells.default = pkgs.mkShell { buildInputs = [ pkgs.virtualEnv ]; };
    });
}

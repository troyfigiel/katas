from unittest.mock import Mock


def test_account_should_store_deposit():
    transaction_repository = Mock()
    account = Account(transaction_repository)
    account.deposit(150)
    assert transaction_repository.calls == []

from unittest.mock import Mock, call

import pytest

from banking.account import Account
from banking.transaction import Transaction


@pytest.fixture()
def statement_formatter():
    return Mock()


@pytest.fixture()
def transaction_repository():
    return Mock()


@pytest.fixture()
def account(transaction_repository, statement_formatter):
    return Account(transaction_repository, statement_formatter)


@pytest.mark.parametrize("amount", [12, 153])
def test_account_should_store_deposit(amount, account, transaction_repository):
    account.deposit(amount)
    assert transaction_repository.mock_calls == [call.store_deposit(amount)]


@pytest.mark.parametrize("amount", [53, 97])
def test_account_should_store_withdrawal(amount, account, transaction_repository):
    account.withdraw(amount)
    assert transaction_repository.mock_calls == [call.store_withdrawal(amount)]


@pytest.mark.parametrize("transactions", [[], [Transaction("12/04/2014", 100)]])
def test_account_should_print_statement(
    account, transaction_repository, statement_formatter, transactions
):
    transaction_repository.retrieve_transactions = Mock(return_value=transactions)
    account.print_statement()
    assert statement_formatter.mock_calls == [call.print_transactions(transactions)]

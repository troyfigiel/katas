import pytest

from banking.format import StatementFormatter
from banking.transaction import Transaction


@pytest.fixture()
def statement_formatter():
    return StatementFormatter()


def test_statement_formatter_should_print_empty_transaction(capfd, statement_formatter):
    statement_formatter.print_transactions([])
    captured = capfd.readouterr()
    assert captured.out.splitlines() == ["DATE | AMOUNT | BALANCE"]


def test_statement_formatter_should_print_single_transaction(
    capfd, statement_formatter
):
    statement_formatter.print_transactions([Transaction("14/03/2019", 250)])
    captured = capfd.readouterr()

    assert captured.out.splitlines() == [
        "DATE | AMOUNT | BALANCE",
        "14/03/2019 | 250 | 250",
    ]

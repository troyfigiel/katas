from banking.account import Account
from banking.format import StatementFormatter
from banking.transaction import TransactionRepository


def test_should_print_statement(capfd):
    transaction_repository = TransactionRepository()
    statement_formatter = StatementFormatter()
    account = Account(transaction_repository, statement_formatter)
    account.deposit(1000)
    account.withdraw(100)
    account.deposit(500)

    account.print_statement()

    captured = capfd.readouterr()
    assert captured.out.splitlines() == [
        "DATE | AMOUNT | BALANCE",
        "10/04/2014 | 500.00 | 1400.00",
        "02/04/2014 | -100.00 | 900.00",
        "01/04/2014 | 1000.00 | 1000.00",
    ]

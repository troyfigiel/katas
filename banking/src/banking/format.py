from banking.transaction import Transaction


def _format_row(*row: str) -> str:
    return " | ".join(row)


class StatementFormatter:
    def print_transactions(self, transactions: list[Transaction]) -> None:
        print(_format_row("DATE", "AMOUNT", "BALANCE"))
        if transactions:
            print(
                _format_row(
                    transactions[0].date,
                    str(transactions[0].amount),
                    str(transactions[0].amount),
                )
            )

from typing import NamedTuple


class Transaction(NamedTuple):
    date: str
    amount: int


class TransactionRepository:
    def store_deposit(self, amount: int) -> None:
        raise NotImplementedError

    def store_withdrawal(self, amount: int) -> None:
        raise NotImplementedError

    def retrieve_transactions(self) -> list[Transaction]:
        return []

from banking.format import StatementFormatter
from banking.transaction import TransactionRepository


class Account:
    def __init__(
        self,
        transaction_repository: TransactionRepository,
        statement_formatter: StatementFormatter,
    ) -> None:
        self.transaction_repository = transaction_repository
        self.statement_formatter = statement_formatter

    def deposit(self, amount: int) -> None:
        self.transaction_repository.store_deposit(amount)

    def withdraw(self, amount: int) -> None:
        self.transaction_repository.store_withdrawal(amount)

    def print_statement(self) -> None:
        self.statement_formatter.print_transactions(
            self.transaction_repository.retrieve_transactions()
        )

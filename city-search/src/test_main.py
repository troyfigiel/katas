import pytest

from main import DATABASE, search


def test_should_at_least_search_with_two_characters():
    assert search("a") == set()


@pytest.mark.parametrize(
    ["s", "expected"],
    [
        ("Va", {"Valencia", "Vancouver"}),
        ("Ista", {"Istanbul"}),
    ],
)
def test_should_return_city_names_starting_with_search_string(s, expected):
    assert search(s) == expected


def test_should_search_case_insensitively():
    assert search("ro") == {"Rome", "Rotterdam"}


def test_should_search_anywhere_in_city_name():
    assert search("ape") == {"Budapest"}


def test_should_return_all_when_searching_glob():
    assert search("*") == DATABASE

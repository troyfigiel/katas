DATABASE = {
    "Paris",
    "Budapest",
    "Skopje",
    "Rotterdam",
    "Valencia",
    "Vancouver",
    "Amsterdam",
    "Vienna",
    "Sydney",
    "New York City",
    "London",
    "Bangkok",
    "Hong Kong",
    "Dubai",
    "Rome",
    "Istanbul",
}


def search(s: str) -> set:
    if s == "*":
        return DATABASE
    if len(s) < 2:
        return set()
    return {name for name in DATABASE if s.lower() in name.lower()}

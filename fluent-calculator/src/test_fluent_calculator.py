import pytest

from fluent_calculator import Calculator


@pytest.fixture
def seeded_calculator():
    return Calculator().seed(19)


def test_when_not_seeded_should_return_zero():
    assert Calculator().result() == 0


def test_when_no_operations_done_should_return_seed():
    assert Calculator().seed(1).result() == 1


@pytest.mark.parametrize(["first_seed", "second_seed"], [(2, 3), (0, 7)])
def test_when_seeded_multiple_times_should_return_first_seed(
    first_seed, second_seed
):
    assert (
        Calculator().seed(first_seed).seed(second_seed).result() == first_seed
    )


def test_when_adding_to_seed_should_return_sum(seeded_calculator):
    assert seeded_calculator.plus(5).result() == 24


def test_when_subtracting_from_seed_should_return_difference(
    seeded_calculator,
):
    assert seeded_calculator.minus(3).result() == 16


def test_when_undoing_after_seed_should_return_seed(seeded_calculator):
    assert seeded_calculator.undo().result() == 19


def test_when_undoing_after_plus_should_return_seed(seeded_calculator):
    assert seeded_calculator.plus(7).undo().result() == 19


def test_when_undoing_after_minus_should_return_seed(seeded_calculator):
    assert seeded_calculator.minus(9).undo().result() == 19


def test_when_undoing_two_pluses_should_return_seed(seeded_calculator):
    assert seeded_calculator.plus(3).plus(7).undo().undo().result() == 19


def test_when_redoing_without_undo_should_return_seed(seeded_calculator):
    assert seeded_calculator.redo().result() == 19


def test_when_redoing_after_undo_and_plus_should_return_sum(
    seeded_calculator,
):
    assert seeded_calculator.plus(5).undo().redo().result() == 24


def test_when_saved_should_not_undo_plus(seeded_calculator):
    assert seeded_calculator.plus(5).save().undo().result() == 24


def test_when_saved_should_not_redo_undo_after_plus(seeded_calculator):
    assert seeded_calculator.plus(5).undo().save().redo().result() == 19

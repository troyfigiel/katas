from typing import Optional


class Calculator:
    def __init__(self) -> None:
        self._result = 0
        self._seeded = False
        self._operations: list[int] = []
        self._undos: list[int] = []

    def result(self) -> int:
        return self._result

    def seed(self, value: int) -> "Calculator":
        if not self._seeded:
            self._result = value
            self._seeded = True
        return self

    def plus(self, value: int) -> "Calculator":
        self._result += value
        self._operations.append(value)
        return self

    def minus(self, value: int) -> "Calculator":
        return self.plus(-value)

    def undo(self) -> "Calculator":
        if self._operations:
            undo_value = self._operations.pop()
            self._result -= undo_value
            self._undos.append(undo_value)
        return self

    def redo(self) -> "Calculator":
        if self._undos:
            self._result += self._undos.pop()
        return self

    def save(self) -> "Calculator":
        self._operations = []
        self._undos = []
        return self

from enum import Enum
from typing import NamedTuple

import numpy as np


class Light(NamedTuple):
    x: int
    y: int


class Action(Enum):
    ON = 1
    OFF = 0


class Grid:
    def __init__(self, size: int) -> None:
        if isinstance(size, int) and size > 0:
            self._lights = np.zeros((size, size), dtype=int)
        else:
            raise ValueError("Size must be a positive integer.")

    def brightness(self) -> int:
        return self._lights.sum()

    def turn_on(
        self,
        upper_left_position: tuple[int, int],
        bottom_right_position: tuple[int, int],
    ) -> None:
        self._act(
            Action.ON,
            Light(*upper_left_position),
            Light(*bottom_right_position),
        )

    def turn_off(
        self,
        upper_left_position: tuple[int, int],
        bottom_right_position: tuple[int, int],
    ) -> None:
        self._act(
            Action.OFF,
            Light(*upper_left_position),
            Light(*bottom_right_position),
        )

    def _act(
        self,
        action: Action,
        upper_left_light: Light,
        bottom_right_light: Light,
    ) -> None:
        self._lights[
            upper_left_light.x : bottom_right_light.x + 1,
            upper_left_light.y : bottom_right_light.y + 1,
        ] = action.value


# if __name__ == "__main__":
#     grid = Grid(size=1000)
#     grid.turn_on((887, 9), (959, 629))
#     grid.turn_on((454, 398), (844, 448))
#     grid.turn_off((539, 243), (559, 965))
#     grid.turn_off((370, 819), (678, 868))
#     grid.turn_off((145, 40), (370, 997))
#     grid.turn_off((301, 3), (808, 453))
#     grid.turn_on((351, 678), (951, 908))
#     grid.toggle((720, 196), (897, 994))
#     grid.toggle((831, 394), (904, 860))
#     print(grid.brightness())

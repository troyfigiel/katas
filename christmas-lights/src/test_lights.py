import pytest

from lights import Grid


@pytest.fixture()
def initial_grid():
    return Grid(size=10)


@pytest.fixture()
def grid_with_lights_on():
    grid = Grid(size=10)
    grid.turn_on((2, 4), (3, 7))
    return grid


@pytest.mark.parametrize("size", [0, "Hello, world!"])
def test_should_only_have_positive_size(size):
    with pytest.raises(ValueError, match="Size must be a positive integer."):
        Grid(size=size)


def test_should_have_no_brightness_initially(initial_grid):
    assert initial_grid.brightness() == 0


@pytest.mark.parametrize(
    ["upper_left", "bottom_right", "expected_brightness"],
    [
        ((0, 1), (0, 2), 2),
        ((1, 2), (3, 2), 3),
        ((2, 3), (4, 4), 6),
    ],
)
def test_should_turn_on_block(
    initial_grid, upper_left, bottom_right, expected_brightness
):
    initial_grid.turn_on(upper_left, bottom_right)
    assert initial_grid.brightness() == expected_brightness


@pytest.mark.parametrize(
    ["upper_left", "bottom_right", "expected_brightness"],
    [
        ((0, 1), (0, 2), 8),
        ((2, 3), (4, 5), 4),
    ],
)
def test_should_turn_off_block(
    grid_with_lights_on, upper_left, bottom_right, expected_brightness
):
    grid_with_lights_on.turn_off(upper_left, bottom_right)
    assert grid_with_lights_on.brightness() == expected_brightness
